﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;



namespace MesozoicConsole
{
    
        
    public class Program
    {
        public static void Main(string[] args)
        {
            
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur franklin = new Dinosaur("franklin", "Triceraptos", 18);
            Dinosaur julgo = new Dinosaur("julgo", "Steakausorus", 7);

            List<Dinosaur> dinosaurs = new List<Dinosaur>();

            Dinosaur henry = new Dinosaur("Henry", 11, "Diplodocus");
            Console.WriteLine(henry.sayHello());
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Console.WriteLine(louis.sayHello());
            Console.WriteLine(henry.sayHello());
            Console.ReadKey();
        }
    }
}



