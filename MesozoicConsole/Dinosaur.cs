﻿using System;

namespace Mesozoic
{
    public class Dinosaur
    {
        protected string name;
        protected static string specie;
        protected int age;

        public Dinosaur(string name, int age, string specie)
        {
            this.name = name;
            this.age = age;
            Dinosaur.specie = specie;
        }
        public string getName()
        {
            return this.name;
        }
        public string getSpecie()
        {
            return Dinosaur.specie;
        }
        public int getAge()
        {
              return this.age;
        }
        public void setName(string name)
        {
            this.name=name;
        }
        public void setSpecie(string specie)
        {
            Dinosaur.specie=specie;
        }
        public void setAge(string age)
        {
            this.name=age;
        } 

        public string hug(Dinosaur dinosaur)
        {
            return string.Format("Je suis {0} et je fais un calin à {1}",this.name,dinosaur.getName());
        }

        public Dinosaur(string name, string specie, int age)
        {
            
            this.name = name;
            Dinosaur.specie = specie;
            this.age = age;

            
        }

        public string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.",this.name,Dinosaur.specie,this.age);
        }

        public string roar()
        {
            return "Grrr";
        }
    }
}