﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    /// <summary>
    /// Description résumée pour HordeTest
    /// </summary>
    [TestClass]
    public class HordeTest
    {
        [TestMethod]
        public void HordeConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Horde dinosaurs = new Horde(dinosaurs.getdinosaures);
            
            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }
    }
}
